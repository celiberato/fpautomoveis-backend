package br.com.celiberato.fp.bootstrap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.celiberato.fp.constants.FPConstants;
import br.com.celiberato.fp.domain.Fabricante;
import br.com.celiberato.fp.domain.Kilometragem;
import br.com.celiberato.fp.domain.Modelo;
import br.com.celiberato.fp.domain.Preco;
import br.com.celiberato.fp.domain.Produto;
import br.com.celiberato.fp.repositories.FabricanteRepository;
import br.com.celiberato.fp.repositories.KilometragemRepository;
import br.com.celiberato.fp.repositories.ModeloRepository;
import br.com.celiberato.fp.repositories.PrecoRepository;
import br.com.celiberato.fp.repositories.ProdutoRepository;
import br.com.celiberato.fp.services.ProdutoService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PUBLIC)
public class DatabaseLoader implements Serializable {

    private Logger log = LogManager.getLogger(DatabaseLoader.class);
    
	@Autowired
	FabricanteRepository fabricanteRepository;

	@Autowired
	KilometragemRepository kilometragemRepository;

	@Autowired
	ModeloRepository modeloRepository;

	@Autowired
	PrecoRepository precoRepository;

	@Autowired
	ProdutoRepository produtoRepository;

	ProdutoService produtoService = new ProdutoService();

	private List<Fabricante> listaFabricantes = new ArrayList<>();
	private List<Modelo> listaModelos = new ArrayList<>();
	private List<Preco> listaPrecos = new ArrayList<>();
	private List<Kilometragem> listaKilometragem = new ArrayList<>();


	public void deleteAll() {
		produtoService.setProdutoRepository(produtoRepository);
		produtoService.setFabricanteRepository(fabricanteRepository);
		produtoService.setModeloRepository(modeloRepository);
		produtoService.setPrecoRepository(precoRepository);
		produtoService.setKilometragemRepository(kilometragemRepository);
		
		produtoRepository.deleteAllInBatch();
		modeloRepository.deleteAllInBatch();
		fabricanteRepository.deleteAllInBatch();
		precoRepository.deleteAllInBatch();
		kilometragemRepository.deleteAllInBatch();
	}

	public void loadAll() {
		deleteAll();
		
		listaFabricantes = createFabricantes();
		listaModelos = createModelos();
		listaPrecos = createPrecos();
		listaKilometragem = createKilometragens();
	}
		
	public Produto builderProduto(
			String modelo, 
			String fabricante, 
			String preco,
			String kilometragem,
			Integer ano,
			Double valor,
			String figura1,
			String figura2,
			String figura3) {
		
		return Produto.builder()//
				.modelo(getModelo(modelo, fabricante))
				.kilometragem(getKilometragem(kilometragem))
				.preco(getPreco(preco))
				.anoFabricacao(ano)
				.valor(valor)
				.figura1(figura1)
				.figura2(figura2)
				.figura3(figura3)
				.build();
	}

	public Modelo getModelo(String modelo, String fabricante) {
		return this.listaModelos.stream()
			.filter(obj -> obj.getNome().equals(modelo) && obj.getFabricante().getNome().equals(fabricante))
			.findAny()
			.get();
	};

	public Fabricante getFabricante(String fabricante) {
		return this.listaFabricantes.stream()
			.filter(obj -> obj.getNome().equals(fabricante))
			.findAny()
			.get();
	};

	public Preco getPreco(String preco) {
		return this.listaPrecos.stream()
			.filter(obj -> obj.getNome().equals(preco))
			.findAny()
			.get();
	};

	public Kilometragem getKilometragem(String kilometragem) {
		return this.listaKilometragem.stream()
			.filter(obj -> obj.getNome().equals(kilometragem))
			.findAny()
			.get();
	};

	public Fabricante builderFabricante(String nome) {	
		return Fabricante.builder()//
				.nome(nome)
				.build();
	}

	public Modelo builderModelo(String nome, String fabricante) {	
		return Modelo.builder()//
				.nome(nome)
				.fabricante(this.getFabricante(fabricante))
				.build();
	}

	public Preco builderPreco(String nome) {	
		return Preco.builder()//
				.nome(nome)
				.build();
	}

	public Kilometragem builderKilometragem(String nome) {	
		return Kilometragem.builder()//
				.nome(nome)
				.build();
	}

	public List<Produto> createProdutos() {
		List<Produto> produtos = new ArrayList<>();
		produtos.add(createProduto(FPConstants.MODELO_ASTRA, FPConstants.FABRICANTE_CHEVROLET, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_VECTRA, FPConstants.FABRICANTE_CHEVROLET, FPConstants.PRECO_50_60, FPConstants.KILOMETRAGEM_30_40, 2018, 31000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_CELTA, FPConstants.FABRICANTE_CHEVROLET, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2017, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_CELTA, FPConstants.FABRICANTE_CHEVROLET, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2017, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_PRISMA, FPConstants.FABRICANTE_CHEVROLET, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2017, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_DODGE, FPConstants.FABRICANTE_DODGE, FPConstants.PRECO_50_60, FPConstants.KILOMETRAGEM_10_20, 2018, 45000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_KA, FPConstants.FABRICANTE_FORD, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_FOCUS, FPConstants.FABRICANTE_FORD, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_FOCUS, FPConstants.FABRICANTE_FORD, FPConstants.PRECO_60_70, FPConstants.KILOMETRAGEM_40_50, 2019, 65000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_LANDROVER, FPConstants.FABRICANTE_LANDROVER, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 45000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_LANDROVER, FPConstants.FABRICANTE_LANDROVER, FPConstants.PRECO_60_70, FPConstants.KILOMETRAGEM_50_60, 2018, 55000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_MINI, FPConstants.FABRICANTE_MINI, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_MITSUBISHI, FPConstants.FABRICANTE_MITSUBISHI, FPConstants.PRECO_70_80, FPConstants.KILOMETRAGEM_20_30, 2020, 75000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_MITSUBISHI, FPConstants.FABRICANTE_MITSUBISHI, FPConstants.PRECO_100_150, FPConstants.KILOMETRAGEM_0_10, 2020, 75000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_RENAULT, FPConstants.FABRICANTE_RENAULT, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_SUZUKI, FPConstants.FABRICANTE_LANDROVER, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_SUZUKI, FPConstants.FABRICANTE_LANDROVER, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_PASSAT, FPConstants.FABRICANTE_VOLSWAGEN, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));
		produtos.add(createProduto(FPConstants.MODELO_GOLF, FPConstants.FABRICANTE_VOLSWAGEN, FPConstants.PRECO_30_40, FPConstants.KILOMETRAGEM_20_30, 2018, 35000d, "https://picsum.photos/250/300", "https://picsum.photos/250/300", "https://picsum.photos/250/300"));

		return produtos;
	}
	
	public Produto createProduto(
			String fabricante, 
			String modelo, 
			String preco,
			String kilometragem,
			Integer ano,
			Double valor,
			String figura1,
			String figura2,
			String figura3) {
		
		Produto produto = builderProduto(
				fabricante, 
				modelo, 
				preco,
				kilometragem,
				ano,
				valor,
				figura1,
				figura2,
				figura3);
				
		produtoRepository.save(produto);
		return produto;
	}

	public List<Fabricante> createFabricantes() {
		List<Fabricante> lista = new ArrayList<>();
		
		lista.add(createFabricante(FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createFabricante(FPConstants.FABRICANTE_DODGE));
		lista.add(createFabricante(FPConstants.FABRICANTE_FORD));
		lista.add(createFabricante(FPConstants.FABRICANTE_LANDROVER));
		lista.add(createFabricante(FPConstants.FABRICANTE_MINI));
		lista.add(createFabricante(FPConstants.FABRICANTE_MITSUBISHI));
		lista.add(createFabricante(FPConstants.FABRICANTE_PEUGEOT));
		lista.add(createFabricante(FPConstants.FABRICANTE_RENAULT));
		lista.add(createFabricante(FPConstants.FABRICANTE_SUZUKI));
		lista.add(createFabricante(FPConstants.FABRICANTE_VOLSWAGEN));

		return lista;
	}
	
	
	public List<Modelo> createModelos() {
		List<Modelo> lista = new ArrayList<>();
		
		lista.add(createModelo(FPConstants.MODELO_CELTA, FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createModelo(FPConstants.MODELO_ASTRA, FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createModelo(FPConstants.MODELO_VECTRA, FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createModelo(FPConstants.MODELO_CELTA, FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createModelo(FPConstants.MODELO_CELTA, FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createModelo(FPConstants.MODELO_PRISMA, FPConstants.FABRICANTE_CHEVROLET));
		lista.add(createModelo(FPConstants.MODELO_DODGE, FPConstants.FABRICANTE_DODGE));
		lista.add(createModelo(FPConstants.MODELO_KA, FPConstants.FABRICANTE_FORD));
		lista.add(createModelo(FPConstants.MODELO_FOCUS, FPConstants.FABRICANTE_FORD));
		lista.add(createModelo(FPConstants.MODELO_FOCUS, FPConstants.FABRICANTE_FORD));
		lista.add(createModelo(FPConstants.MODELO_LANDROVER, FPConstants.FABRICANTE_LANDROVER));
		lista.add(createModelo(FPConstants.MODELO_LANDROVER, FPConstants.FABRICANTE_LANDROVER));
		lista.add(createModelo(FPConstants.MODELO_MINI, FPConstants.FABRICANTE_MINI));
		lista.add(createModelo(FPConstants.MODELO_MITSUBISHI, FPConstants.FABRICANTE_MITSUBISHI));
		lista.add(createModelo(FPConstants.MODELO_MITSUBISHI, FPConstants.FABRICANTE_MITSUBISHI));
		lista.add(createModelo(FPConstants.MODELO_RENAULT, FPConstants.FABRICANTE_RENAULT));
		lista.add(createModelo(FPConstants.MODELO_SUZUKI, FPConstants.FABRICANTE_LANDROVER));
		lista.add(createModelo(FPConstants.MODELO_SUZUKI, FPConstants.FABRICANTE_LANDROVER));
		lista.add(createModelo(FPConstants.MODELO_PASSAT, FPConstants.FABRICANTE_VOLSWAGEN));
		lista.add(createModelo(FPConstants.MODELO_GOLF, FPConstants.FABRICANTE_VOLSWAGEN));
		
		return lista;
	}
	
	public List<Preco> createPrecos() {
		List<Preco> lista = new ArrayList<>();
		
		lista.add(createPreco(FPConstants.PRECO_10_20));
		lista.add(createPreco(FPConstants.PRECO_20_30));
		lista.add(createPreco(FPConstants.PRECO_30_40));
		lista.add(createPreco(FPConstants.PRECO_40_50));
		lista.add(createPreco(FPConstants.PRECO_50_60));
		lista.add(createPreco(FPConstants.PRECO_60_70));
		lista.add(createPreco(FPConstants.PRECO_70_80));
		lista.add(createPreco(FPConstants.PRECO_80_90));
		lista.add(createPreco(FPConstants.PRECO_90_100));
		lista.add(createPreco(FPConstants.PRECO_100_150));
		
		return lista;
	}
	
	public List<Kilometragem> createKilometragens() {
		List<Kilometragem> lista = new ArrayList<>();
		
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_0_10));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_10_20));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_20_30));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_30_40));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_40_50));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_50_60));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_60_70));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_70_80));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_80_90));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_90_100));
		lista.add(createKilometragem(FPConstants.KILOMETRAGEM_100_200));

		return lista;
	}
	
	
	public Fabricante createFabricante(String nome) {
		Fabricante fabricante = builderFabricante(nome);
		
		fabricanteRepository.save(fabricante);
		return fabricante;
	}

	public Modelo createModelo(String nome, String fabricante) {
		Modelo modelo = builderModelo(nome, fabricante);
		
		if(!modeloRepository.findByNome(modelo.getNome()).isPresent()) {
			modeloRepository.save(modelo);
		}
		return modelo;
	}

	public Preco createPreco(String nome) {
		Preco preco = builderPreco(nome);
		
		if(!precoRepository.findByNome(preco.getNome()).isPresent()) {
			precoRepository.save(preco);
		}
		return preco;
	}

	public Kilometragem createKilometragem(String nome) {
		Kilometragem kilometragem = builderKilometragem(nome);
		
		if(!kilometragemRepository.findByNome(kilometragem.getNome()).isPresent()) {
			kilometragemRepository.save(kilometragem);
		}
		return kilometragem;
	}


}
