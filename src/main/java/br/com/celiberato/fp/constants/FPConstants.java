package br.com.celiberato.fp.constants;

public class FPConstants {

	// fabricantes ----------------------
	public static final String FABRICANTE_CHEVROLET 	= "CHEVROLET";
	public static final String FABRICANTE_DODGE 		= "DODGE";
	public static final String FABRICANTE_FORD 			= "FORD";
	public static final String FABRICANTE_LANDROVER 	= "LANDROVER";
	public static final String FABRICANTE_MINI 			= "MINI";
	public static final String FABRICANTE_MITSUBISHI 	= "MITSUBISHI";
	public static final String FABRICANTE_PEUGEOT 		= "PEUGEOT";
	public static final String FABRICANTE_RENAULT 		= "RENAULT";
	public static final String FABRICANTE_SUZUKI 		= "SUZUKI";
	public static final String FABRICANTE_VOLSWAGEN 	= "VOLKSWAGEN";
	
	// modelos ---------------------------

	// CHEVROLET
	public static final String MODELO_ASTRA 	= "ASTRA";
	public static final String MODELO_VECTRA 	= "VECTRA";
	public static final String MODELO_GOL 		= "GOL";
	public static final String MODELO_CELTA 	= "CELTA";
	public static final String MODELO_PRISMA 	= "PRISMA";
	
	// DODGE
	public static final String MODELO_DODGE 	= "DODGE";

	// FORD
	public static final String MODELO_KA 		= "KA";
	public static final String MODELO_FOCUS 	= "FOCUS";

	// LANDROVER
	public static final String MODELO_LANDROVER 	= "LANDROVER";

	// MINI 
	public static final String MODELO_MINI 	= "MINI";
	
	//MITSUBISHI
	public static final String MODELO_MITSUBISHI 	= "MITSUBISHI";
	
	//PEUGEOT
	public static final String MODELO_PEUGEOT 	= "PEUGEOT";
	
	// RENAULT
	public static final String MODELO_RENAULT 	= "RENAULT";
	
	// SUZUKI
	public static final String MODELO_SUZUKI 	= "SUZUKI";
	
	// VOLKSWAGEN
	public static final String MODELO_PASSAT 	= "PASSAT";
	public static final String MODELO_GOLF 		= "GOLF";

	
	// faixa de preço -----------------------
	public static final String PRECO_10_20 		= "R$ 10K - R$ 20K";
	public static final String PRECO_20_30 		= "R$ 20K - R$ 30K";
	public static final String PRECO_30_40 		= "R$ 30K - R$ 40K";
	public static final String PRECO_40_50 		= "R$ 40K - R$ 50K";
	public static final String PRECO_50_60 		= "R$ 50K - R$ 60K";
	public static final String PRECO_60_70 		= "R$ 60K - R$ 70K";
	public static final String PRECO_70_80 		= "R$ 70K - R$ 80K";
	public static final String PRECO_80_90 		= "R$ 80K - R$ 90K";
	public static final String PRECO_90_100		= "R$ 90K - R$ 100K";
	public static final String PRECO_100_150 	= "R$ 100K - R$ 150K";


	// kilometragem -------------------------
	public static final String KILOMETRAGEM_0_10 		= "0 - 10K";
	public static final String KILOMETRAGEM_10_20 		= "10K - 20K";
	public static final String KILOMETRAGEM_20_30 		= "20K - 30K";
	public static final String KILOMETRAGEM_30_40 		= "30K - 40K";
	public static final String KILOMETRAGEM_40_50 		= "40K - 50K";
	public static final String KILOMETRAGEM_50_60 		= "50K - 60K";
	public static final String KILOMETRAGEM_60_70 		= "60K - 70K";
	public static final String KILOMETRAGEM_70_80 		= "70K - 80K";
	public static final String KILOMETRAGEM_80_90 		= "80K - 90K";
	public static final String KILOMETRAGEM_90_100 		= "90K - 100K";
	public static final String KILOMETRAGEM_100_200 	= "100K - 200K";

	
}
