package br.com.celiberato.fp.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MessagesConstants {

	public static final String PRODUTO_INEXISTENTE = "Produto não existe no banco de dados!";
	public static final String FABRICANTE_INEXISTENTE = "Fabricante não existe no banco de dados!";
	public static final String MODELO_INEXISTENTE = "Modelo não existe no banco de dados";
	public static final String PRECO_INEXISTENTE = "Preço não existe no banco de dados!";
	public static final String KILOMETRAGEM_INEXISTENTE = "Kilometragem não existe no banco de dados!";

	public static final String PRODUTO_REQUERIDO = "Produto é requerido!";
	public static final String FABRICANTE_REQUERIDO = "Fabricante é requerido!";
	public static final String MODELO_REQUERIDO = "Modelo é requerido!";
	public static final String PRECO_REQUERIDO = "Preço é requerido!";
	public static final String KILOMETRAGEM_REQUERIDO = "Kilometragem é requerido!";
	
}
