package br.com.celiberato.fp.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.fp.domain.Modelo;
import br.com.celiberato.fp.domain.dto.IdentifierDTO;
import br.com.celiberato.fp.domain.dto.ModeloDTO;
import br.com.celiberato.fp.domain.vo.FiltroModeloVO;
import br.com.celiberato.fp.domain.vo.ModeloVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.services.ModeloService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/modelos", produces = { MediaType.APPLICATION_JSON_VALUE})
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ModeloController extends BaseController {


	@Autowired
	ModeloService modeloService;

	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public IdentifierDTO create(@RequestBody @Valid final ModeloVO vo, @ApiIgnore Principal principal) throws BusinessException {
		Modelo modelo = convertToEntity(vo, Modelo.class);

		return new IdentifierDTO(modeloService.create(modelo).getId());
	}

	@DeleteMapping(ID_PATH_VARIABLE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@ApiParam(value = "Id do modelo", required = true) @PathVariable(required = true) final Long id, @ApiIgnore Principal principal) throws BusinessException {
		modeloService.delete(id);
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Página a ser retornada (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Número de endereços por página") //
	})
	public Page<ModeloDTO> findByFiltro(FiltroModeloVO filtro, @ApiIgnore Pageable pageable) throws BusinessException {
		return convertToDTO(modeloService.findByFiltro(filtro, pageable), ModeloDTO.class);
	}

	@GetMapping(value = "/all")
	@ResponseStatus(HttpStatus.OK)
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Página a ser retornada (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Número de endereços por página") //
	})
	public Page<ModeloDTO> findAll(@ApiIgnore Pageable pageable) throws BusinessException {
		return convertToDTO(modeloService.findByFiltro(new FiltroModeloVO(), pageable), ModeloDTO.class);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ModeloDTO findById(@ApiParam(value = "Identificador único", required = true) @PathVariable(required = true) final Long id) throws BusinessException {
		return convertToDTO(modeloService.findByIdFecthAll(id).get(), ModeloDTO.class);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Atualizar Modelo")
	public void update(@RequestBody @Valid final ModeloVO vo, @PathVariable(required = true) final Long id) throws BusinessException {
		Modelo modelo = convertToEntity(vo, id, Modelo.class);

		modeloService.update(modelo);
	}

}
