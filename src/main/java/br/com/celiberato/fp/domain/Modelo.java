package br.com.celiberato.fp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "NOME", name = "UK_MODELO_NOME"))
public class Modelo extends AbstractEntity<Long> {

	static final long serialVersionUID = 1l;

	@Column(name = "NOME")
	private String nome;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FABRICANTE", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_MODELO_FABRICANTE"))
	Fabricante fabricante;

	public Modelo(String nome, String nomeFabricante) {
		this.nome = nome;
		this.fabricante = new Fabricante(nomeFabricante);
	}
}