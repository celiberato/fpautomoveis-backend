package br.com.celiberato.fp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
@Entity
public class Produto extends AbstractEntity<Long> {

	static final long serialVersionUID = 1l;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_MODELO", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PRODUTO_MODELO"))
	Modelo modelo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PRECO", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PRODUTO_PRECO"))
	Preco preco;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_KILOMETRAGEM", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PRODUTO_KILOMETRAGEM"))
	Kilometragem kilometragem;;
	
	@Column(name = "ANO_FABRICACAO")
	private Integer anoFabricacao;
	
	@Column(name = "VALOR")
	private Double valor;
	
	@Column(name = "FIGURA_1")
	private String figura1;

	@Column(name = "FIGURA_2")
	private String figura2;

	@Column(name = "FIGURA_3")
	private String figura3;

}