package br.com.celiberato.fp.domain.vo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FiltroModeloVO {

	Long idModelo;

	Long idFabricante;

	String nome;

	public FiltroModeloVO(Long idModelo) {
		this.idModelo = idModelo;	
	}
}
