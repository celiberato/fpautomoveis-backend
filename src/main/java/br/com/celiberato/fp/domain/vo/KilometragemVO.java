package br.com.celiberato.fp.domain.vo;

import java.io.Serializable;

import br.com.celiberato.fp.domain.AbstractEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class KilometragemVO implements Serializable {

	static final long serialVersionUID = 1l;

	private Long id;
	private String nome;
	

}