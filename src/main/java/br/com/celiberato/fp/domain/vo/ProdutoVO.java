package br.com.celiberato.fp.domain.vo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.celiberato.fp.domain.Fabricante;
import br.com.celiberato.fp.domain.Kilometragem;
import br.com.celiberato.fp.domain.Modelo;
import br.com.celiberato.fp.domain.Preco;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ProdutoVO implements Serializable {

	static final long serialVersionUID = 1l;

	private Long id;
	private ModeloVO modelo;
	private Integer anoFabricacao;
	private PrecoVO preco;
	private KilometragemVO kilometragem;;
	private Double valor;
	private String figura1;
	private String figura2;
	private String figura3;

}