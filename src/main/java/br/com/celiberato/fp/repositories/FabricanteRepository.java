package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.celiberato.fp.domain.Fabricante;

public interface FabricanteRepository extends JpaRepository<Fabricante, Long>, FabricanteRepositoryCustom {

	@Query(value = "select f from Fabricante f where f.nome = :nome")
	Optional<Fabricante> findByNome(@Param("nome") String nome);
}
