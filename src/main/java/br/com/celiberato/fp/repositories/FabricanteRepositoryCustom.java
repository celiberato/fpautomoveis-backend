package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.celiberato.fp.domain.Fabricante;
import br.com.celiberato.fp.domain.vo.FiltroFabricanteVO;

public interface FabricanteRepositoryCustom {

	Optional<Fabricante> findByIdFecthAll(Long id);
	
	Page<Fabricante> findByFiltro(FiltroFabricanteVO filtro, Pageable pageable);
}
