package br.com.celiberato.fp.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.celiberato.fp.domain.Fabricante;
import br.com.celiberato.fp.domain.Fabricante_;
import br.com.celiberato.fp.domain.vo.FiltroFabricanteVO;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@FieldDefaults(level = AccessLevel.PROTECTED)
public class FabricanteRepositoryCustomImpl implements FabricanteRepositoryCustom {

	@Autowired
	EntityManager entityManager;
	
	private Long countResult(final FiltroFabricanteVO filtro) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cBuilder.createQuery(Long.class);
		Root<Fabricante> root = cq.from(Fabricante.class);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Long> countSelect = cq.select(cBuilder.count(root.get(Fabricante_.id)));
		countSelect.where(predicates.toArray(new Predicate[predicates.size()]));

		return entityManager.createQuery(countSelect).getSingleResult();

	}

	private List<Predicate> createPredicateFindByFilter(final FiltroFabricanteVO filtro, CriteriaBuilder cb, Root<Fabricante> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (Objects.nonNull(filtro.getIdFabricante())) {
			predicates.add(cb.equal(root.get(Fabricante_.id), filtro.getIdFabricante()));
		}

		if (Objects.nonNull(filtro.getNome())) {
			predicates.add(cb.like(cb.lower(root.get(Fabricante_.nome)), '%' + filtro.getNome().toLowerCase() + '%'));
		}

		return predicates;
	}

	@Override
	public Page<Fabricante> findByFiltro(FiltroFabricanteVO filtro, Pageable pageable) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fabricante> cQuery = cBuilder.createQuery(Fabricante.class);

		Root<Fabricante> root = cQuery.from(Fabricante.class);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Fabricante> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		cQuery.orderBy(cBuilder.desc(root.get(Fabricante_.nome)));

		Long count = countResult(filtro);
		if (count == 0) {
			return new PageImpl<>(Collections.emptyList(), pageable, count);
		}

		TypedQuery<Fabricante> query = entityManager.createQuery(select);
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		return new PageImpl<>(query.getResultList(), pageable, count);
	}

	@Override
	public Optional<Fabricante> findByIdFecthAll(Long id) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fabricante> cQuery = cBuilder.createQuery(Fabricante.class);

		Root<Fabricante> root = cQuery.from(Fabricante.class);

		FiltroFabricanteVO filtro = new FiltroFabricanteVO(id);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);
		CriteriaQuery<Fabricante> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Fabricante> query = entityManager.createQuery(select);

		try {
			return Optional.ofNullable(query.getSingleResult());

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}

		return Optional.empty();
	}

}
