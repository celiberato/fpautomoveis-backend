package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.celiberato.fp.domain.Kilometragem;
import br.com.celiberato.fp.domain.Preco;

@Repository
public interface KilometragemRepository extends JpaRepository<Kilometragem, Long>, KilometragemRepositoryCustom {

	@Query(value = "select k from Kilometragem k where k.nome = :nome")
	Optional<Preco> findByNome(@Param("nome") String nome);
}
