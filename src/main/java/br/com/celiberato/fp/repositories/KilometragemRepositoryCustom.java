package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.celiberato.fp.domain.Kilometragem;
import br.com.celiberato.fp.domain.vo.FiltroKilometragemVO;

public interface KilometragemRepositoryCustom {

	Optional<Kilometragem> findByIdFecthAll(Long id);
	
	Page<Kilometragem> findByFiltro(FiltroKilometragemVO filtro, Pageable pageable);
}
