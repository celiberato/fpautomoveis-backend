package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.celiberato.fp.domain.Modelo;

public interface ModeloRepository extends JpaRepository<Modelo, Long>, ModeloRepositoryCustom {

	@Query(value = "select f from Modelo f where f.nome = :nome")
	Optional<Modelo> findByNome(@Param("nome") String nome);
}
