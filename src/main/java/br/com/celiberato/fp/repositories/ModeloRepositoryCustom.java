package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.celiberato.fp.domain.Modelo;
import br.com.celiberato.fp.domain.vo.FiltroModeloVO;

public interface ModeloRepositoryCustom {

	Optional<Modelo> findByIdFecthAll(Long id);
	
	Page<Modelo> findByFiltro(FiltroModeloVO filtro, Pageable pageable);
}
