package br.com.celiberato.fp.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.celiberato.fp.domain.Fabricante_;
import br.com.celiberato.fp.domain.Modelo;
import br.com.celiberato.fp.domain.Modelo_;
import br.com.celiberato.fp.domain.vo.FiltroModeloVO;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ModeloRepositoryCustomImpl implements ModeloRepositoryCustom {

	@Autowired
	EntityManager entityManager;
	
	private Long countResult(final FiltroModeloVO filtro) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cBuilder.createQuery(Long.class);
		Root<Modelo> root = cq.from(Modelo.class);
		root.join(Modelo_.fabricante, JoinType.LEFT);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Long> countSelect = cq.select(cBuilder.count(root.get(Fabricante_.id)));
		countSelect.where(predicates.toArray(new Predicate[predicates.size()]));

		return entityManager.createQuery(countSelect).getSingleResult();

	}

	private List<Predicate> createPredicateFindByFilter(final FiltroModeloVO filtro, CriteriaBuilder cb, Root<Modelo> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (Objects.nonNull(filtro.getIdModelo())) {
			predicates.add(cb.equal(root.get(Modelo_.id), filtro.getIdModelo()));
		}

		if (Objects.nonNull(filtro.getNome())) {
			predicates.add(cb.like(cb.lower(root.get(Modelo_.nome)), '%' + filtro.getNome().toLowerCase() + '%'));
		}

		return predicates;
	}

	@Override
	public Page<Modelo> findByFiltro(FiltroModeloVO filtro, Pageable pageable) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Modelo> cQuery = cBuilder.createQuery(Modelo.class);

		Root<Modelo> root = cQuery.from(Modelo.class);
		root.fetch(Modelo_.fabricante, JoinType.LEFT);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Modelo> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		cQuery.orderBy(cBuilder.desc(root.get(Modelo_.nome)));

		Long count = countResult(filtro);
		if (count == 0) {
			return new PageImpl<>(Collections.emptyList(), pageable, count);
		}

		TypedQuery<Modelo> query = entityManager.createQuery(select);
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		return new PageImpl<>(query.getResultList(), pageable, count);
	}

	@Override
	public Optional<Modelo> findByIdFecthAll(Long id) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Modelo> cQuery = cBuilder.createQuery(Modelo.class);

		Root<Modelo> root = cQuery.from(Modelo.class);
		root.fetch(Modelo_.fabricante, JoinType.LEFT);
		
		FiltroModeloVO filtro = new FiltroModeloVO(id);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);
		CriteriaQuery<Modelo> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Modelo> query = entityManager.createQuery(select);

		try {
			return Optional.ofNullable(query.getSingleResult());

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}

		return Optional.empty();
	}

}
