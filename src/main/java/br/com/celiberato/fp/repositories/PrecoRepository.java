package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.celiberato.fp.domain.Kilometragem;
import br.com.celiberato.fp.domain.Preco;

@Repository
public interface PrecoRepository extends JpaRepository<Preco, Long>, PrecoRepositoryCustom {

	@Query(value = "select k from Preco k where k.nome = :nome")
	Optional<Preco> findByNome(@Param("nome") String nome);
}
