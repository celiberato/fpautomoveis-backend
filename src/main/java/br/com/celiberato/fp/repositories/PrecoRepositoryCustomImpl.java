package br.com.celiberato.fp.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.celiberato.fp.domain.Fabricante_;
import br.com.celiberato.fp.domain.Kilometragem_;
import br.com.celiberato.fp.domain.Modelo_;
import br.com.celiberato.fp.domain.Preco_;
import br.com.celiberato.fp.domain.Preco;
import br.com.celiberato.fp.domain.Preco_;
import br.com.celiberato.fp.domain.vo.FiltroPrecoVO;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@FieldDefaults(level = AccessLevel.PROTECTED)
public class PrecoRepositoryCustomImpl implements PrecoRepositoryCustom {

	@Autowired
	EntityManager entityManager;
	
	private Long countResult(final FiltroPrecoVO filtro) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cBuilder.createQuery(Long.class);
		Root<Preco> root = cq.from(Preco.class);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Long> countSelect = cq.select(cBuilder.count(root.get(Preco_.id)));
		countSelect.where(predicates.toArray(new Predicate[predicates.size()]));

		return entityManager.createQuery(countSelect).getSingleResult();

	}

	private List<Predicate> createPredicateFindByFilter(final FiltroPrecoVO filtro, CriteriaBuilder cb, Root<Preco> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (Objects.nonNull(filtro.getIdPreco())) {
			predicates.add(cb.equal(root.get(Preco_.id), filtro.getIdPreco()));
		}

		return predicates;
	}

	@Override
	public Page<Preco> findByFiltro(FiltroPrecoVO filtro, Pageable pageable) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Preco> cQuery = cBuilder.createQuery(Preco.class);

		Root<Preco> root = cQuery.from(Preco.class);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Preco> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		cQuery.orderBy(cBuilder.asc(root.get(Preco_.id)));

		Long count = countResult(filtro);
		if (count == 0) {
			return new PageImpl<>(Collections.emptyList(), pageable, count);
		}

		TypedQuery<Preco> query = entityManager.createQuery(select);
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		return new PageImpl<>(query.getResultList(), pageable, count);
	}

	@Override
	public Optional<Preco> findByIdFecthAll(Long id) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Preco> cQuery = cBuilder.createQuery(Preco.class);

		Root<Preco> root = cQuery.from(Preco.class);

		FiltroPrecoVO filtro = new FiltroPrecoVO(id);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);
		CriteriaQuery<Preco> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Preco> query = entityManager.createQuery(select);

		try {
			return Optional.ofNullable(query.getSingleResult());

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}

		return Optional.empty();
	}

}
