package br.com.celiberato.fp.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.celiberato.fp.domain.Produto;
import br.com.celiberato.fp.domain.vo.FiltroProdutoVO;

public interface ProdutoRepositoryCustom {

	Optional<Produto> findByIdFecthAll(Long id);
	
	Page<Produto> findByFiltro(FiltroProdutoVO filtro, Pageable pageable);
}
