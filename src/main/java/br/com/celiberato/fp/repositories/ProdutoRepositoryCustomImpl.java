package br.com.celiberato.fp.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.celiberato.fp.domain.Fabricante_;
import br.com.celiberato.fp.domain.Kilometragem_;
import br.com.celiberato.fp.domain.Modelo_;
import br.com.celiberato.fp.domain.Preco_;
import br.com.celiberato.fp.domain.Produto;
import br.com.celiberato.fp.domain.Produto_;
import br.com.celiberato.fp.domain.vo.FiltroProdutoVO;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ProdutoRepositoryCustomImpl implements ProdutoRepositoryCustom {

	@Autowired
	EntityManager entityManager;
	
	private Long countResult(final FiltroProdutoVO filtro) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cBuilder.createQuery(Long.class);
		Root<Produto> root = cq.from(Produto.class);

		root.join(Produto_.modelo, JoinType.LEFT).join(Modelo_.fabricante, JoinType.LEFT);
		root.join(Produto_.preco, JoinType.LEFT);
		root.join(Produto_.kilometragem, JoinType.LEFT);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Long> countSelect = cq.select(cBuilder.count(root.get(Produto_.id)));
		countSelect.where(predicates.toArray(new Predicate[predicates.size()]));

		return entityManager.createQuery(countSelect).getSingleResult();

	}

	private List<Predicate> createPredicateFindByFilter(final FiltroProdutoVO filtro, CriteriaBuilder cb, Root<Produto> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (Objects.nonNull(filtro.getIdProduto())) {
			predicates.add(cb.equal(root.get(Produto_.id), filtro.getIdProduto()));
		}

		if (Objects.nonNull(filtro.getIdFabricante())) {
			predicates.add(cb.equal(root.get(Produto_. modelo).get(Modelo_.fabricante).get(Fabricante_.id), filtro.getIdFabricante()));
		}

		if (Objects.nonNull(filtro.getIdModelo())) {
			predicates.add(cb.equal(root.get(Produto_.modelo).get(Modelo_.id), filtro.getIdModelo()));
		}

		if (Objects.nonNull(filtro.getIdPreco())) {
			predicates.add(cb.equal(root.get(Produto_.preco).get(Preco_.id), filtro.getIdPreco()));
		}

		if (Objects.nonNull(filtro.getIdKilometragem())) {
			predicates.add(cb.equal(root.get(Produto_.kilometragem).get(Kilometragem_.id), filtro.getIdKilometragem()));
		}

		if (Objects.nonNull(filtro.getNome())) {
			predicates.add(cb.like(cb.lower(root.get(Produto_.modelo).get(Modelo_.nome)), '%' + filtro.getNome().toLowerCase() + '%'));
		}

		return predicates;
	}

	@Override
	public Page<Produto> findByFiltro(FiltroProdutoVO filtro, Pageable pageable) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Produto> cQuery = cBuilder.createQuery(Produto.class);

		Root<Produto> root = cQuery.from(Produto.class);
		root.fetch(Produto_.modelo, JoinType.LEFT).fetch(Modelo_.fabricante, JoinType.LEFT);
		root.fetch(Produto_.preco, JoinType.LEFT);
		root.fetch(Produto_.kilometragem, JoinType.LEFT);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Produto> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		cQuery.orderBy(cBuilder.desc(root.get(Produto_.modelo).get(Modelo_.nome)));

		Long count = countResult(filtro);
		if (count == 0) {
			return new PageImpl<>(Collections.emptyList(), pageable, count);
		}

		TypedQuery<Produto> query = entityManager.createQuery(select);
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		return new PageImpl<>(query.getResultList(), pageable, count);
	}

	@Override
	public Optional<Produto> findByIdFecthAll(Long id) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Produto> cQuery = cBuilder.createQuery(Produto.class);

		Root<Produto> root = cQuery.from(Produto.class);
		root.fetch(Produto_.modelo, JoinType.LEFT).fetch(Modelo_.fabricante, JoinType.LEFT);
		root.fetch(Produto_.preco, JoinType.LEFT);
		root.fetch(Produto_.kilometragem, JoinType.LEFT);

		FiltroProdutoVO filtro = new FiltroProdutoVO(id);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);
		CriteriaQuery<Produto> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Produto> query = entityManager.createQuery(select);

		try {
			return Optional.ofNullable(query.getSingleResult());

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}

		return Optional.empty();
	}

}
