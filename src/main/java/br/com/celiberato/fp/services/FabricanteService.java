package br.com.celiberato.fp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.fp.constants.MessagesConstants;
import br.com.celiberato.fp.domain.Fabricante;
import br.com.celiberato.fp.domain.vo.FiltroFabricanteVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.exception.RecordNotFoundException;
import br.com.celiberato.fp.repositories.FabricanteRepository;
import br.com.celiberato.fp.repositories.KilometragemRepository;
import br.com.celiberato.fp.repositories.ModeloRepository;
import br.com.celiberato.fp.repositories.PrecoRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FabricanteService {

    @Autowired
	private FabricanteRepository fabricanteRepository;

	public Fabricante create(Fabricante fabricante) throws BusinessException {

    	validate(fabricante);
    	
		return fabricanteRepository.save(fabricante);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			fabricanteRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.FABRICANTE_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Fabricante> findByFiltro(FiltroFabricanteVO filtro, Pageable pageable) {
		return fabricanteRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Fabricante> findByIdFecthAll(final Long id) throws RecordNotFoundException{
		return fabricanteRepository.findByIdFecthAll(id);
	}


	public Fabricante update(final Fabricante fabricante) throws BusinessException {

    	validate(fabricante);

    	if (!fabricanteRepository.existsById(fabricante.getId())) {
			throw new RecordNotFoundException();
		}

		return fabricanteRepository.save(fabricante);
	}

	
	public void validate(Fabricante fabricante) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(fabricante.getId()) && !fabricanteRepository.existsById(fabricante.getId())){
			throw new BusinessException(MessagesConstants.FABRICANTE_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

	}
}
