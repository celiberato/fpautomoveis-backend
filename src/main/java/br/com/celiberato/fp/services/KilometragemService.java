package br.com.celiberato.fp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.fp.constants.MessagesConstants;
import br.com.celiberato.fp.domain.Kilometragem;
import br.com.celiberato.fp.domain.vo.FiltroKilometragemVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.exception.RecordNotFoundException;
import br.com.celiberato.fp.repositories.KilometragemRepository;
import br.com.celiberato.fp.repositories.ProdutoRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class KilometragemService {

    @Autowired
	private ProdutoRepository produtoRepository;

    @Autowired
	private KilometragemRepository kilometragemRepository;

	public Kilometragem create(Kilometragem kilometragem) throws BusinessException {

    	validate(kilometragem);
    	
		return kilometragemRepository.save(kilometragem);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			kilometragemRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.KILOMETRAGEM_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Kilometragem> findByFiltro(FiltroKilometragemVO filtro, Pageable pageable) {
		return kilometragemRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Kilometragem> findByIdFecthAll(final Long id) throws RecordNotFoundException{
		return kilometragemRepository.findByIdFecthAll(id);
	}


	public Kilometragem update(final Kilometragem kilometragem) throws BusinessException {

    	validate(kilometragem);

    	if (!kilometragemRepository.existsById(kilometragem.getId())) {
			throw new RecordNotFoundException();
		}

		return kilometragemRepository.save(kilometragem);
	}

	
	public void validate(Kilometragem kilometragem) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(kilometragem.getId()) && !kilometragemRepository.existsById(kilometragem.getId())){
			throw new BusinessException(MessagesConstants.KILOMETRAGEM_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
	}
}
