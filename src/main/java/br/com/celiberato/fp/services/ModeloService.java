package br.com.celiberato.fp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.fp.constants.MessagesConstants;
import br.com.celiberato.fp.domain.Modelo;
import br.com.celiberato.fp.domain.vo.FiltroModeloVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.exception.RecordNotFoundException;
import br.com.celiberato.fp.repositories.ModeloRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ModeloService {

    @Autowired
	private ModeloRepository modeloRepository;

	public Modelo create(Modelo modelo) throws BusinessException {

    	validate(modelo);
    	
		return modeloRepository.save(modelo);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			modeloRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.MODELO_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Modelo> findByFiltro(FiltroModeloVO filtro, Pageable pageable) {
		return modeloRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Modelo> findByIdFecthAll(final Long id) throws RecordNotFoundException{
		return modeloRepository.findByIdFecthAll(id);
	}


	public Modelo update(final Modelo modelo) throws BusinessException {

    	validate(modelo);

    	if (!modeloRepository.existsById(modelo.getId())) {
			throw new RecordNotFoundException();
		}

		return modeloRepository.save(modelo);
	}
	
	public void validate(Modelo modelo) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(modelo.getId()) && !modeloRepository.existsById(modelo.getId())){
			throw new BusinessException(MessagesConstants.MODELO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

	}
}
