package br.com.celiberato.fp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.fp.constants.MessagesConstants;
import br.com.celiberato.fp.domain.Preco;
import br.com.celiberato.fp.domain.vo.FiltroPrecoVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.exception.RecordNotFoundException;
import br.com.celiberato.fp.repositories.PrecoRepository;
import br.com.celiberato.fp.repositories.ProdutoRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PrecoService {

    @Autowired
	private ProdutoRepository produtoRepository;

    @Autowired
	private PrecoRepository precoRepository;

	public Preco create(Preco preco) throws BusinessException {

    	validate(preco);
    	
		return precoRepository.save(preco);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			precoRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.PRECO_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Preco> findByFiltro(FiltroPrecoVO filtro, Pageable pageable) {
		return precoRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Preco> findByIdFecthAll(final Long id) throws RecordNotFoundException{
		return precoRepository.findByIdFecthAll(id);
	}


	public Preco update(final Preco preco) throws BusinessException {

    	validate(preco);

    	if (!precoRepository.existsById(preco.getId())) {
			throw new RecordNotFoundException();
		}

		return precoRepository.save(preco);
	}

	
	public void validate(Preco preco) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(preco.getId()) && !precoRepository.existsById(preco.getId())){
			throw new BusinessException(MessagesConstants.PRECO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
	}
}
