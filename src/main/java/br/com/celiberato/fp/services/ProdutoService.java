package br.com.celiberato.fp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.fp.constants.MessagesConstants;
import br.com.celiberato.fp.domain.Produto;
import br.com.celiberato.fp.domain.vo.FiltroProdutoVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.exception.RecordNotFoundException;
import br.com.celiberato.fp.repositories.FabricanteRepository;
import br.com.celiberato.fp.repositories.KilometragemRepository;
import br.com.celiberato.fp.repositories.ModeloRepository;
import br.com.celiberato.fp.repositories.PrecoRepository;
import br.com.celiberato.fp.repositories.ProdutoRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProdutoService {

    @Autowired
	private ProdutoRepository produtoRepository;

    @Autowired
	private FabricanteRepository fabricanteRepository;

    @Autowired
	private ModeloRepository modeloRepository;

    @Autowired
	private PrecoRepository precoRepository;

    @Autowired
	private KilometragemRepository kilometragemRepository;

	public Produto create(Produto produto) throws BusinessException {

    	validate(produto);
    	
		return produtoRepository.save(produto);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			produtoRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.PRODUTO_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Produto> findByFiltro(FiltroProdutoVO filtro, Pageable pageable) {
		return produtoRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Produto> findByIdFecthAll(final Long id) throws RecordNotFoundException{
		return produtoRepository.findByIdFecthAll(id);
	}


	public Produto update(final Produto produto) throws BusinessException {

    	validate(produto);

    	if (!produtoRepository.existsById(produto.getId())) {
			throw new RecordNotFoundException();
		}

		return produtoRepository.save(produto);
	}

	
	public void validate(Produto produto) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(produto.getId()) && !produtoRepository.existsById(produto.getId())){
			throw new BusinessException(MessagesConstants.PRODUTO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		
		if(ObjectUtils.isEmpty(produto.getModelo()) || ObjectUtils.isEmpty(produto.getModelo().getId())){
			throw new BusinessException(MessagesConstants.MODELO_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!modeloRepository.existsById(produto.getModelo().getId())) {
			throw new BusinessException(MessagesConstants.MODELO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		
		if(ObjectUtils.isEmpty(produto.getPreco()) || ObjectUtils.isEmpty(produto.getPreco().getId())){
			throw new BusinessException(MessagesConstants.PRECO_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!precoRepository.existsById(produto.getPreco().getId())) {
			throw new BusinessException(MessagesConstants.PRECO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

		if(ObjectUtils.isEmpty(produto.getKilometragem()) || ObjectUtils.isEmpty(produto.getKilometragem().getId())){
			throw new BusinessException(MessagesConstants.KILOMETRAGEM_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!kilometragemRepository.existsById(produto.getKilometragem().getId())) {
			throw new BusinessException(MessagesConstants.KILOMETRAGEM_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

	}
}
