package br.com.celiberato.fp.tests.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.testcontainers.shaded.org.apache.commons.lang.StringUtils;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public abstract class AbstractControllerIT {

	protected ModelMapper modelMapper =  new ModelMapper();

	protected <D, T> List<D> convertToDTO(final Iterable<T> models, final Class<D> dtoClass) {
		List<D> dtos = new ArrayList<>();
		for (T model : models) {
			dtos.add(modelMapper.map(model, dtoClass));
		}

		return dtos;
	}

	protected <D, T> List<D> convertToVO(final Iterable<T> models, final Class<D> voClass) {
		List<D> dtos = new ArrayList<>();
		for (T model : models) {
			dtos.add(modelMapper.map(model, voClass));
		}

		return dtos;
	}
	public <D, T> D convertToDTO(final T model, final Class<D> dtoClass) {
		return modelMapper.map(model, dtoClass);
	}

	public <V, T> V convertToVO(final T model, final Class<V> voClass) {
		return modelMapper.map(model, voClass);
	}
	
	public HttpHeaders getHeaders(final String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + token);

		return headers;
	}
}
