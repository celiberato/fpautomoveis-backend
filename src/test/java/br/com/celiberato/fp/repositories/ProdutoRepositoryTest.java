package br.com.celiberato.fp.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.celiberato.fp.bootstrap.DatabaseLoader;
import br.com.celiberato.fp.configuration.RepositoryConfiguration;
import br.com.celiberato.fp.constants.MessagesConstants;
import br.com.celiberato.fp.domain.Produto;
import br.com.celiberato.fp.domain.dto.ProdutoDTO;
import br.com.celiberato.fp.domain.vo.FiltroProdutoVO;
import br.com.celiberato.fp.domain.vo.ProdutoVO;
import br.com.celiberato.fp.exception.BusinessException;
import br.com.celiberato.fp.services.ProdutoService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {RepositoryConfiguration.class})
public class ProdutoRepositoryTest extends BaseControllerIT {

	public static final String HTTP = "http";
	public static final String HOST = "localhost";
	public static final Integer PORT = 3025;
	public static final String BASE_URL = "/produtos/";

    private ModelMapper modelMapper =  new ModelMapper();

	static final String RESOURCE_BASE_URL = "";

	TestRestTemplate restTemplate =  new TestRestTemplate(new RestTemplateBuilder().rootUri("http://localhost:3024/produtos/"));

	ProdutoService produtoService = new ProdutoService();

	DatabaseLoader databaseLoader = new DatabaseLoader();

	@Autowired
	ProdutoRepository produtoRepository;

	@Autowired
	FabricanteRepository fabricanteRepository;

	@Autowired
	ModeloRepository modeloRepository;

	@Autowired
	PrecoRepository precoRepository;

	@Autowired
	KilometragemRepository kilometragemRepository;


	@Before
	public void setUp() {
		produtoService.setProdutoRepository(produtoRepository);
		produtoService.setFabricanteRepository(fabricanteRepository);
		produtoService.setModeloRepository(modeloRepository);
		produtoService.setPrecoRepository(precoRepository);
		produtoService.setKilometragemRepository(kilometragemRepository);

		databaseLoader.setProdutoService(produtoService);
		databaseLoader.setProdutoRepository(produtoRepository);
		databaseLoader.setFabricanteRepository(fabricanteRepository);
		databaseLoader.setModeloRepository(modeloRepository);
		databaseLoader.setPrecoRepository(precoRepository);
		databaseLoader.setKilometragemRepository(kilometragemRepository);

		databaseLoader.deleteAll();
		
		databaseLoader.loadAll();
	}
	
	
	@Test	
	public void testDeleteOK() throws BusinessException, URISyntaxException {
		ProdutoVO produto = convertToVO(databaseLoader.createProdutos().get(0), ProdutoVO.class);
		assertTrue(produtoRepository.existsById(produto.getId()));
		
		produtoRepository.deleteById(produto.getId());
		assertFalse(produtoRepository.existsById(produto.getId()));
	}	

	@Test
	public void testDeleteNotFound() throws BusinessException, URISyntaxException {
		try {
			produtoRepository.deleteById(99999L);
			assertTrue(false);
		} catch (EmptyResultDataAccessException e) {
			assertTrue(true);
		}
	}


	@Test
	public void testFindById() throws BusinessException, URISyntaxException {
		ProdutoVO produto = convertToVO(databaseLoader.createProdutos().get(0), ProdutoVO.class);

		Optional<Produto> result = produtoRepository.findByIdFecthAll(produto.getId());
		
		assertTrue(result.isPresent());
		assertEqualsToProduto(modelMapper.map(produto, ProdutoDTO.class), modelMapper.map(result.get(), ProdutoDTO.class));
	}


	@Test
	public void testFindByModelo() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idModelo(produtos.get(0).getModelo().getId())
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		for (Produto response : result.getContent()) {
			assertEqualsToProduto(modelMapper.map(produtos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), ProdutoDTO.class), modelMapper.map(response, ProdutoDTO.class));
		}
	}

	@Test
	public void testFindByPreco() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idPreco(produtos.get(0).getPreco().getId())
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		for (Produto response : result.getContent()) {
			assertEqualsToProduto(modelMapper.map(produtos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), ProdutoDTO.class), modelMapper.map(response, ProdutoDTO.class));
		}
	}

	@Test
	public void testFindByKilometragem() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idKilometragem(produtos.get(0).getKilometragem().getId())
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		for (Produto response : result.getContent()) {
			assertEqualsToProduto(modelMapper.map(produtos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), ProdutoDTO.class), modelMapper.map(response, ProdutoDTO.class));
		}
	}	
	@Test
	public void testFindByAllFilters() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idModelo(produtos.get(0).getModelo().getId())
				.idPreco(produtos.get(0).getPreco().getId())
				.idKilometragem(produtos.get(0).getKilometragem().getId())
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		for (Produto response : result.getContent()) {
			assertEqualsToProduto(modelMapper.map(produtos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), ProdutoDTO.class), modelMapper.map(response, ProdutoDTO.class));
		}
	}


	@Test
	public void testFindByFabricanteInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idFabricante(99999L)
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}

	@Test
	public void testFindByModeloInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idModelo(99999L)
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}

	@Test
	public void testFindByPrecoInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idPreco(99999L)
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}

	@Test
	public void testFindByKilometragemInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<ProdutoVO> produtos = convertToVO(databaseLoader.createProdutos(), ProdutoVO.class);

		FiltroProdutoVO filtro = FiltroProdutoVO.builder()
				.idKilometragem(99999L)
				.build();
		
		Page<Produto> result = produtoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}
	


	void assertEqualsToProduto(final ProdutoDTO expected, final ProdutoDTO actual) {
		assertEquals(expected.getId(), actual.getId());
		
		if(expected.getModelo().getFabricante()!=null && actual.getModelo().getFabricante()!=null){
			assertEquals(expected.getModelo().getFabricante().getId(), actual.getModelo().getFabricante().getId());
			assertEquals(expected.getModelo().getFabricante().getNome(), actual.getModelo().getFabricante().getNome());
		}

		if(expected.getModelo()!=null && actual.getModelo()!=null){
			assertEquals(expected.getModelo().getId(), actual.getModelo().getId());
			assertEquals(expected.getModelo().getNome(), actual.getModelo().getNome());
		}

		if(expected.getPreco()!=null && actual.getPreco()!=null){
			assertEquals(expected.getPreco().getId(), actual.getPreco().getId());
			assertEquals(expected.getPreco().getNome(), actual.getPreco().getNome());
		}

		if(expected.getKilometragem()!=null && actual.getKilometragem()!=null){
			assertEquals(expected.getKilometragem().getId(), actual.getKilometragem().getId());
			assertEquals(expected.getKilometragem().getNome(), actual.getKilometragem().getNome());
		}

		assertEquals(expected.getAnoFabricacao(), actual.getAnoFabricacao());
		assertEquals(expected.getValor(), actual.getValor());
		assertEquals(expected.getFigura1(), actual.getFigura1());
		assertEquals(expected.getFigura2(), actual.getFigura2());
		assertEquals(expected.getFigura3(), actual.getFigura3());
	}

}
